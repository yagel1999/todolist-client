import React from 'react';
import { useRecoilValue } from 'recoil';
import { filteredTodoList } from 'store';
import { ITodo } from 'models';
import Todo from './Todo';
import './todos.scss';

const TodosContainer: React.FC = () => {
	const todos: ITodo[] = useRecoilValue(filteredTodoList);

	return todos.length ? (
		<div className='todolist-container'>
			{todos.map((todo: ITodo) => (
				<Todo key={todo.id} todo={todo} />
			))}
		</div>
	) : (
		<div className='no-todos'>Sorry! No Todos currently</div>
	);
};

export default TodosContainer;
