import React from 'react';
import { VscTrash } from 'react-icons/vsc';
import { ITodo, Status } from 'models';
import { useUpdateTodo, useDeleteTodo } from 'store';
import TodoName from './TodoName';
import './todo.scss';

interface Props {
	todo: ITodo;
}

const Todo: React.FC<Props> = React.memo(({ todo }: Props) => {
	const updateHandler = useUpdateTodo();
	const deleteTodoHandler = useDeleteTodo();

	const deleteHandler = (): void => {
		deleteTodoHandler(todo.id);
	};

	const toggleStatusHandler = (): void => {
		updateHandler({
			...todo,
			status: 1 - todo.status
		});
	};

	return (
		<div
			className={`todo-container ${
				todo.status === Status.Active ? 'active-todo' : 'completed-todo'
			}`}
		>
			<div className='todo-name'>
				<input
					type='checkbox'
					readOnly
					checked={todo.status === Status.Completed}
					className='toggle-button'
					onClick={toggleStatusHandler}
				/>
				<TodoName todo={todo} />
			</div>
			<button
				type='button'
				className='button delete-button'
				onClick={deleteHandler}
			>
				<VscTrash />
			</button>
		</div>
	);
});

export default Todo;
