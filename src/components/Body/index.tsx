import React from 'react';
import ActionsBar from './ActionsBar';
import TodosContainer from './Todos';

const Body: React.FC = () => {
	return (
		<>
			<ActionsBar />
			<TodosContainer />
		</>
	);
};

export default Body;
