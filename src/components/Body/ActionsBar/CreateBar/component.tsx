import React, { useRef } from 'react';
import './create-bar.scss';

interface Props {
	submit: (todoName: string) => void;
}

const CreateTodoComponent: React.FC<Props> = ({ submit }: Props) => {
	const refToInput = useRef<HTMLInputElement>(null);

	const submitHandler = (): void => {
		if (refToInput.current && refToInput.current.value !== '') {
			submit(refToInput.current.value);
			refToInput.current.value = '';
		}
	};

	const onEnter = (event: React.KeyboardEvent<HTMLInputElement>): void => {
		if (event.key === 'Enter') {
			submitHandler();
		}
	};

	return (
		<div className='create-bar'>
			<input
				type='text'
				className='input-bar'
				ref={refToInput}
				placeholder='Todo Name'
				onKeyDown={onEnter}
			/>
			<button type='button' className='submit-button' onClick={submitHandler}>
				Add
			</button>
		</div>
	);
};

export default CreateTodoComponent;
