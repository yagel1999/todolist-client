import React, { useCallback } from 'react';
import { useAddTodo } from 'store';
import { ITodoInput, Status } from 'models';
import CreateTodoComponent from './component';

const CreateTodoContainer: React.FC = () => {
	const add = useAddTodo();

	const submit = useCallback(
		(todoName: string): void => {
			const newTodo: ITodoInput = {
				title: todoName,
				status: Status.Active
			};

			add(newTodo);
		},
		[add]
	);

	return <CreateTodoComponent submit={submit} />;
};

export default CreateTodoContainer;
