import { ITodo, ITodoInput } from './ITodo';

export default interface IAPI {
	create: (todo: ITodoInput) => Promise<ITodo>;
	update: (todo: ITodo) => Promise<boolean>;
	delete: (todoID: string) => Promise<boolean>;
	getById: (todoID: string) => Promise<ITodo>;
	getAll: () => Promise<ITodo[]>;
}

export type Mapper<T> = (oldTodo: T) => ITodo;
