'use-strict';

const path = require('path');
const { merge } = require('webpack-merge');
const { ProgressPlugin } = require('webpack');
const DotenvPlugin = require('dotenv-webpack');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const ReactRefreshWebpackPlugin = require('@pmmmwh/react-refresh-webpack-plugin');
const commonConfig = require('./webpack.common');

module.exports = merge(commonConfig, {
	mode: 'development',
	target: 'web',
	devtool: 'eval-source-map',
	devServer: {
		port: 3000,
		open: true,
		hot: true,
		compress: true,
		overlay: true
	},
	plugins: [
		new ProgressPlugin(),
		new ReactRefreshWebpackPlugin(),
		new ForkTsCheckerWebpackPlugin(),
		new DotenvPlugin({ 
			path: path.join(__dirname, 'dev.env'),
		})
	]
});
