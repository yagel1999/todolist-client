'use-strict';

const path = require('path');
const { merge } = require('webpack-merge');
const DotenvPlugin = require('dotenv-webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const commonConfig = require('./webpack.common');

module.exports = merge(commonConfig, {
	mode: 'production',
	target: 'browserslist',
	output: {
		path: path.join(__dirname, '..', 'dist'),
		filename: 'bundle.[contenthash].js'
	},
	optimization: {
		minimize: true,
		minimizer: [
			() => ({
				cache: true,
				parallel: true,
				extractComments: false,
				terserOptions: {
					output: {
						comments: false
					}
				}
			})
		]
	},
	plugins: [
		new CleanWebpackPlugin(),
		new DotenvPlugin({ 
			path: path.join(__dirname, 'prod.env'),
		})
	]
});
